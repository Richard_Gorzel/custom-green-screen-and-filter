import processing.video.*;

Capture camera;
PImage mask;
int threshold = 60;
float hue = 0;
PImage AirOrange;
PImage PerfectGreen;
int greenBias = 10;
int brokenFrames = 0;
int offsetrandomness = 15;
int breakchance = 3;
int brokenlimit = 10;

void setup()
{
  size(320,240);
  println("Available Cameras:");
  printArray(Capture.list());
  camera = new Capture(this,width,height,30);
  camera.start();
  AirOrange = new PImage(width,height);
  PerfectGreen = new PImage(width,height);
  for(int i = 0; i < AirOrange.width * AirOrange.height; i++)
  {
    AirOrange.pixels[i] = color(255,77,0);
    PerfectGreen.pixels[i] = color(0,255,0);
  }
}

int getIndex(int x, int y, int w)
{
  return x + y * w;
}

int edgeBrightness(Capture camera, int x, int y)
{
  int brightness = int(map(0,1,0,255,abs(brightness(camera.pixels[getIndex(x-1,y-1,camera.width)])-brightness(camera.pixels[getIndex(x+1,y+1,camera.width)]))));
  brightness += int(map(0,1,0,255,abs(brightness(camera.pixels[getIndex(x-1,y,camera.width)])-brightness(camera.pixels[getIndex(x+1,y,camera.width)]))));
  brightness += int(map(0,1,0,255,abs(brightness(camera.pixels[getIndex(x-1,y+1,camera.width)])-brightness(camera.pixels[getIndex(x+1,y-1,camera.width)]))));
  brightness += int(map(0,1,0,255,abs(brightness(camera.pixels[getIndex(x,y-1,camera.width)])-brightness(camera.pixels[getIndex(x,y+1,camera.width)]))));
  return brightness;
}

PImage detectEdge(Capture camera)
{
  PImage output = new PImage(camera.width, camera.height);
  for(int x = 0; x < camera.width; x++)
  {
    output.pixels[getIndex(x,0,camera.width)] = color(0,0,0,0);
    output.pixels[getIndex(x,camera.height - 1,camera.width)] = color(0,0,0,0);
  }
  brokenFrames = brokenFrames <= 0?random(100) < breakchance?int(random(brokenlimit)):0:brokenFrames;
  for(int y = 1; y < camera.height - 1; y++)
  {
    int offset = brokenFrames > 0?int(random(offsetrandomness)):-1;
    offset -= offset < 0?0:offsetrandomness / 2;
    for(int x = 1; x < camera.width - 1; x++)
    {
      int brightness = edgeBrightness(camera,x,y);
      int alpha = 255;
      if(green(camera.pixels[getIndex(x,y,camera.width)]) > greenBias + red(camera.pixels[getIndex(x,y,camera.width)]) && green(camera.pixels[getIndex(x,y,camera.width)]) > greenBias + blue(camera.pixels[getIndex(x,y,camera.width)]))
      {
        alpha = 0;
      }
      //brightness = brightness > threshold?255:0;
      output.pixels[getIndex(constrain(int(x+offset),1,camera.width-1),y,output.width)] = color(brightness, brightness, brightness, alpha);
    }
  }
  output.pixels[0] = color(0,0,0,255);
  brokenFrames -= 1;
  return output;
}

PImage maskOut(Capture camera, PImage mask)
{
  PImage output = new PImage(camera.width,camera.height);
  output.pixels = camera.pixels;
  output.pixels[0] = color(red(camera.pixels[0]),green(camera.pixels[0]),blue(camera.pixels[0]),255);
  for(int i = 1; i < camera.width * camera.height; i++)
  {
    output.pixels[i] = color(red(camera.pixels[i]),green(camera.pixels[i]),blue(camera.pixels[i]),red(mask.pixels[i]));
  }
  return output;
}

PImage maskOut(PImage image, PImage mask)
{
  PImage output = new PImage(image.width,image.height);
  output.pixels = image.pixels;
  output.pixels[0] = color(red(image.pixels[0]),green(image.pixels[0]),blue(image.pixels[0]),255);
  for(int i = 1; i < image.width * image.height; i++)
  {
    output.pixels[i] = color(red(image.pixels[i]),green(image.pixels[i]),blue(image.pixels[i]),red(mask.pixels[i]));
  }
  return output;
}

void draw()
{
  if(camera.available())
  {
    //colorMode(HSB,255);
    //hue += (hue > 255)?-255:1;
    //background(color(hue,255,255));
    //colorMode(RGB,255);
    camera.read();
    camera.loadPixels();
    image(PerfectGreen,0,0,width,height);
    image(detectEdge(camera),0,0,width,height);
  }
}
