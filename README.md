# Custom Green Screen and Filter

This project masks out all objects that are within a certain color tolerance and applies an edge detect operation on all remaining pixels. There is also an element of randomness added in the form of a random offset for each row for a random duration with a random time between 'glitches'.

This project only works on linux at the moment, and uses the camera with the highest id, so there may be issues with getting a specific camera to work, but with my setup it is very reliable.
